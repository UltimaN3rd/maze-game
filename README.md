# Maze Game

By Nicholas Walton. This is side-project I started working on for fun. Generates a maze with a cool light-casting effect.

All required libraries are bundled in the repository.

Uses several Croaking Kero libraries which are in the following repo: https://gitlab.com/UltimaN3rd/croaking-kero-c-libraries

And Stretchy Buffer: https://github.com/nothings/stb/blob/master/stretchy_buffer.h

Links to Xlib on Linux and SDL2 on Windows/Mac. I'll add actual build instructions and stuff later if I get this little project to a good state.

![Preview gif](preview.gif)
