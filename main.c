#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include "stretchy_buffer.h"
#include "kero_math.h"
#include "kero_platform.h"
#include "kero_sprite.h"
#include "kero_vec2.h"
#include "kero_font.h"

#define MAZE_WIDTH 26
#define MAZE_HEIGHT 14
#define GRID_CELL_SIZE 44
#define DRAW_OFFSETX 20
#define DRAW_OFFSETY 20
#define LIGHT_RADIUS 300

#include "kero_maze.h"

#define Swap(type, a, b) do{ type t = a; (a) = (b), (b) = (t); } while(0)

#define MAX_PROFILE_TIMES 16
double profile_times[MAX_PROFILE_TIMES];
char profile_time_names[MAX_PROFILE_TIMES][64];
int current_profile_time = 0;
#define MAX_PROFILE_FRAMES 60
int current_profile_frame = 0;
uint32_t profile_colours[MAX_PROFILE_TIMES];
typedef struct{
    int num_profiles;
    double profiles[MAX_PROFILE_TIMES];
} profile_frame_t;
profile_frame_t profile_frames[MAX_PROFILE_FRAMES] = {0};
profile_frame_t new_profile_frame = {0};

kfont_t font;

void ProfileTime(char* name){
    if(current_profile_time >= MAX_PROFILE_TIMES) return;
    profile_times[current_profile_time] = KPClock();
    strcpy(profile_time_names[current_profile_time], name);
    ++current_profile_time;
}

struct {
    vec2i_t pos;
} player;

struct {
    vec2i_t pos;
} mouse;

bool LoadSprite(ksprite_t* sprite, char* filepath){
    if(!KSLoad(sprite, filepath)){
        printf("Failed to load sprite: %s\n", filepath);
        return false;
    }
    return true;
}

void SortRays(int f, int l, float* ray_angles, vec2_t* rays){
    int i, j, p = 0;
    float tf;
    vec2_t tr;
    if (f < l) {
        p = f, i = f, j = l;
        while (i < j) {
            while (ray_angles[i] <= ray_angles[p] && i < l)
                i++;
            while (ray_angles[j] > ray_angles[p])
                j--;
            if (i < j) {
                Swap(float, ray_angles[i], ray_angles[j]);
                Swap(vec2_t, rays[i], rays[j]);
            }
        }
        Swap(float, ray_angles[p], ray_angles[j]);
        Swap(vec2_t, rays[p], rays[j]);
        SortRays(f, j - 1, ray_angles, rays);
        SortRays(j + 1, l, ray_angles, rays);
    }
}

bool GenerateMazeEventCallback(ksprite_t* frame_buffer, bool* disable_animation){
    while(KPEventsQueued()){
        kp_event_t* e = KPNextEvent();
        switch(e->type){
            case KPEVENT_KEY_PRESS:{
                switch(e->key){
                    case KEY_ESCAPE:{
                        return false;
                    }break;
                    case KEY_SPACE:{
                        *disable_animation = true;
                    }break;
                }
            }break;
            case KPEVENT_RESIZE:{
                frame_buffer->w = kp_frame_buffer.w;
                frame_buffer->h = kp_frame_buffer.h;
                frame_buffer->pixels = kp_frame_buffer.pixels;
            }break;
            case KPEVENT_QUIT:{
                return false;
            }break;
        }
        KPFreeEvent(e);
    }
    return true;
}

void DrawTriToView(KMask* dest, ksprite_t* pixels, vec2_t* rays, int a, int b) {
    float x0 = rays[a].x + LIGHT_RADIUS;
    float y0 = rays[a].y + LIGHT_RADIUS;
    float x1 = rays[b].x + LIGHT_RADIUS;
    float y1 = rays[b].y + LIGHT_RADIUS;
    float x2 = LIGHT_RADIUS;
    float y2 = LIGHT_RADIUS;
    // Sort vertices vertically so v0y <= v1y <= v2y
    if(y0 > y1){
        Swap(float, x0, x1);
        Swap(float, y0, y1);
    }
    if(y1 > y2){
        Swap(float, x2, x1);
        Swap(float, y2, y1);
    }
    if(y0 > y1){
        Swap(float, x0, x1);
        Swap(float, y0, y1);
    }
    if(y1>y0){
        float maxy = Max(0, Min(dest->h-1, y1));
        for(int y = Max(0, Min(dest->h-1, y0)); y < maxy; ++y){
            float x01 = Max(0, Min(dest->w-1, x0+(x1-x0)*((Min(Max((float)y,y0),y1)-y0)/(y1-y0))));
            float x02 = Max(0, Min(dest->w-1, x0+(x2-x0)*((Min(Max((float)y,y0),y1)-y0)/(y2-y0))));
            float minx = Min(x01, x02);
            float maxx = Max(x01, x02);
            uint8_t* pixel_it = dest->pixels + y*dest->w + (int)minx;
            uint32_t* src_pixel_it = pixels->pixels + y*dest->w + (int)minx;
            for(int x = minx; x <= maxx; ++x) {
                *(pixel_it++) = (uint8_t)*(src_pixel_it++);
            }
        }
    }
    if(y2>y1){
        float maxy = Max(0, Min(dest->h-1, y2));
        for(int y = Max(0, Min(dest->h-1, y1)); y < maxy; ++y){
            float x02 = Max(0, Min(dest->w-1, x0+(x2-x0)*((Min(Max((float)y,y1),y2)-y0)/(y2-y0))));
            float x12 = Max(0, Min(dest->w-1, x1+(x2-x1)*((Min(Max((float)y,y1),y2)-y1)/(y2-y1))));
            float minx = Min(x02, x12);
            float maxx = Max(x02, x12);
            uint8_t* pixel_it = dest->pixels + y*dest->w + (int)minx;
            uint32_t* src_pixel_it = pixels->pixels + y*dest->w + (int)minx;
            for(int x = minx; x <= maxx; ++x) {
                *(pixel_it++) = (uint8_t)*(src_pixel_it++);
            }
        }
    }
}

int main(void){
    KPInit(1280, 720, "Maze game");
    srand(time(0));
    ksprite_t frame_buffer;
    ksprite_t kero_sprite;
    ksprite_t trophy_sprite;
    ksprite_t maze_sprite;
    ksprite_t maze_sprite_grey;
    KSCreate(&maze_sprite, MAZE_WIDTH*GRID_CELL_SIZE+1, MAZE_HEIGHT*GRID_CELL_SIZE+1);
    KSCreate(&maze_sprite_grey, MAZE_WIDTH*GRID_CELL_SIZE+1, MAZE_HEIGHT*GRID_CELL_SIZE+1);
    ksprite_t fog_of_war;
    KSCreate(&fog_of_war, MAZE_WIDTH*GRID_CELL_SIZE+1, MAZE_HEIGHT*GRID_CELL_SIZE+1);
    KSSetAllPixels(&fog_of_war, 0);
    vec2i_t trophy_pos;
    wall_t* walls = NULL;
    maze_t maze = {0};
    ksprite_t light_sprite;
    KMask view_mask;
    ksprite_t view_sprite;
    if(!LoadSprite(&kero_sprite, (char*)"kero.png")){return -1;}
    if(!LoadSprite(&trophy_sprite, (char*)"croakingkeroglow.png")){return -1;}
    if(!KFLoad(&font, "font.png")){return -1;}
    frame_buffer.w = kp_frame_buffer.w;
    frame_buffer.h = kp_frame_buffer.h;
    frame_buffer.pixels = kp_frame_buffer.pixels;
    KSSetAllPixels(&frame_buffer, 0xff884444);
#define GENERATE_MAZE { \
        KPSetTargetFramerate(0); \
        MazeGenerate(&maze, &walls, MAZE_WIDTH, MAZE_HEIGHT, GRID_CELL_SIZE, &frame_buffer);\
        unsigned int* weights;\
        MazeFindSolution(&maze, &weights);\
        free(weights);\
        player.pos = maze.start;\
        trophy_pos = maze.end;\
        KPSetTargetFramerate(0);\
        /*for(int y = 0; y < maze.h; y++){\
            for(int x = 0; x < maze.w; x++){\
                MazeDrawCell(&maze, x, y, &maze_sprite);\
            }\
        }\*/KSSetAllPixels(&maze_sprite, 0xff880088);\
        for(int y = 0; y < maze_sprite.h/16; y+=1) {\
            int x = y%2;\
            for(; x < maze_sprite.w/16; x+=2) {\
                KSDrawRectFilled(&maze_sprite, x*(GRID_CELL_SIZE/2), y*(GRID_CELL_SIZE/2), (x+1)*(GRID_CELL_SIZE/2), (y+1)*(GRID_CELL_SIZE/2), 0xff0000bb);\
            }\
        }\
        for(int wall = 0; wall < sb_count(walls); ++wall){\
            KSDrawRectFilled(&maze_sprite, walls[wall].a.x-1,walls[wall].a.y-1, walls[wall].b.x,walls[wall].b.y, 0xffffffff);\
        }\
        KSBlit(&maze_sprite, &maze_sprite_grey, 0, 0);\
        KSToGreyScale(&maze_sprite_grey);\
        KSSetAllPixels(&fog_of_war, 0);\
    }
    GENERATE_MAZE;
    KSCreate(&light_sprite, LIGHT_RADIUS*2, LIGHT_RADIUS*2);
    KMCreate(&view_mask, LIGHT_RADIUS*2, LIGHT_RADIUS*2, 0xff);
    for(int y = 0; y < LIGHT_RADIUS*2; ++y){
        for(int x = 0; x < LIGHT_RADIUS*2; ++x){
            float brightness = 1.f - Min(1, (LineLength(LIGHT_RADIUS, LIGHT_RADIUS, x, y) / (float)LIGHT_RADIUS));
            uint8_t grey = 255.f * brightness;
            KSSetPixel(&light_sprite, x, y, (grey << 16) + (grey << 8) + grey + (255 << 24));
        }
    }
    KSCreate(&view_sprite, LIGHT_RADIUS*2, LIGHT_RADIUS*2);
    
    profile_colours[0] = 0xff888888;
    profile_colours[1] = 0xffff0000;
    profile_colours[2] = 0xff00ff00;
    profile_colours[3] = 0xff0000ff;
    profile_colours[4] = 0xffffff00;
    profile_colours[5] = 0xff00ffff;
    profile_colours[6] = 0xffff00ff;
    profile_colours[7] = 0xffffffff;
    profile_colours[8] = 0xff88ff00;
    profile_colours[9] = 0xffff8800;
    profile_colours[10] = 0xff8800ff;
    profile_colours[11] = 0xffff0088;
    profile_colours[12] = 0xff00ff88;
    profile_colours[13] = 0xff44bb44;
    profile_colours[14] = 0xffbb44bb;
    profile_colours[15] = 0xff4444ff;
    strcpy(profile_time_names[0], "Frame start");
    
    bool draw_mode_walls = true;
    bool game_running = true;
    bool draw_profiles = true;
    while(game_running){
        ProfileTime("Frame start");
        
        while(KPEventsQueued()){
            kp_event_t* e = KPNextEvent();
            switch(e->type){
                case KPEVENT_KEY_PRESS:{
                    switch(e->key){
                        /*case KEY_LEFT:{
                        if(player.pos.x/maze.cell_size > 0 &&
                        maze.cells[(player.pos.y/maze.cell_size)*maze.w + player.pos.x/maze.cell_size] & Left){
                        player.pos.x -= maze.cell_size;
                        }
                        }break;
                        case KEY_RIGHT:{
                        if(player.pos.x/maze.cell_size < maze.w - 1 &&
                        maze.cells[(player.pos.y/maze.cell_size)*maze.w + player.pos.x/maze.cell_size] & Right){
                        player.pos.x += maze.cell_size;
                        }
                        }break;
                        case KEY_UP:{
                        if(player.pos.y/maze.cell_size > 0 &&
                        maze.cells[(player.pos.y/maze.cell_size)*maze.w + player.pos.x/maze.cell_size] & Up){
                        player.pos.y -= maze.cell_size;
                        }
                        }break;
                        case KEY_DOWN:{
                        if(player.pos.y/maze.cell_size < maze.h-1 &&
                        maze.cells[(player.pos.y/maze.cell_size)*maze.w + player.pos.x/maze.cell_size] & Down){
                        player.pos.y += maze.cell_size;
                        }
                        }break;*/
                        case KEY_ESCAPE:{
                            game_running = false;
                        }break;
                        case KEY_SPACE:{
                            draw_mode_walls = !draw_mode_walls;
                        }break;
                        case KEY_R:{
                            GENERATE_MAZE;
                        }break;
                        case KEY_P:{
                            draw_profiles = !draw_profiles;
                        }break;
                    }
                }break;
                case KPEVENT_MOUSE_BUTTON_PRESS:{
                    switch(e->button){
                        case MOUSE_LEFT:{
                        }break;
                        case MOUSE_RIGHT:{
                        }break;
                    }
                }break;
                case KPEVENT_MOUSE_BUTTON_RELEASE:{
                    switch(e->button){
                        case MOUSE_LEFT:{
                        }break;
                        case MOUSE_RIGHT:{
                        }break;
                    }
                }break;
                case KPEVENT_RESIZE:{
                    frame_buffer.w = kp_frame_buffer.w;
                    frame_buffer.h = kp_frame_buffer.h;
                    frame_buffer.pixels = kp_frame_buffer.pixels;
                }break;
                case KPEVENT_QUIT:{
                    game_running = false;
                }break;
            }
            KPFreeEvent(e);
        }
        mouse.pos.x = kp_mouse.x;
        mouse.pos.y = kp_mouse.y;
        
        int px = (player.pos.x+20) / maze.cell_size;
        int py = (player.pos.y+21) / maze.cell_size;
        if(kp_keyboard[KEY_LEFT]){
            player.pos.x -= 200.f*kp_delta;
            if((player.pos.x+5) / maze.cell_size < px && !(maze.cells[py*maze.w + px] & MAZE_LEFT)){
                player.pos.x = px * maze.cell_size - 4;
            }
        }
        if(kp_keyboard[KEY_RIGHT]){
            player.pos.x += 200.f*kp_delta;
            if((player.pos.x+37)/maze.cell_size > px && !(maze.cells[py*maze.w + px] & MAZE_RIGHT)){
                player.pos.x = (px+1) * maze.cell_size -37;
            }
        }
        if(kp_keyboard[KEY_UP]){
            player.pos.y -= 200.f*kp_delta;
            if((player.pos.y+5) / maze.cell_size < py && !(maze.cells[py*maze.w + px] & MAZE_DOWN)){
                player.pos.y = py * maze.cell_size - 4;
            }
        }
        if(kp_keyboard[KEY_DOWN]){
            player.pos.y += 200.f*kp_delta;
            if((player.pos.y+37) / maze.cell_size > py && !(maze.cells[py*maze.w + px] & MAZE_UP)){
                player.pos.y = (py+1) * maze.cell_size - 37;
            }
        }
        player.pos.x = Min(maze.w*maze.cell_size-37, Max(0, player.pos.x));
        player.pos.y = Min(maze.h*maze.cell_size-37, Max(0, player.pos.y));
        if(maze.end.y == (player.pos.y+20)/maze.cell_size && maze.end.x == (player.pos.x+20)/maze.cell_size){
            GENERATE_MAZE;
        }
        
        ProfileTime("Inputs + player movement");
        vec2_t* rays = NULL;
        vec2_t vec_player_pos = Vec2Make(player.pos.x+20, player.pos.y+21);
        vec2_t ray_offset = {
            280 - player.pos.x,
            279 - player.pos.y
        };
#define MAX_RAY_LENGTH 850
        float* ray_angles = 0;
        for(int wall_it = 0; wall_it < sb_count(walls); ++wall_it){
            vec2_t intersection0 = Vec2Make(walls[wall_it].a.x, walls[wall_it].a.y);
            vec2_t intersection1 = Vec2Make(walls[wall_it].b.x, walls[wall_it].b.y);
            float angle0 = Vec2Angle(Vec2Sub(intersection0, vec_player_pos));
            float angle1 = Vec2Angle(Vec2Sub(intersection1, vec_player_pos));
            if(angle1 < angle0 || angle1 > angle0 + PI){
                float anglet = angle0; angle0 = angle1; angle1 = anglet;
                vec2_t intersectiont = intersection0; intersection0 = intersection1; intersection1 = intersectiont;
            }
            vec2_t intersection0_off = Vec2Add(Vec2Rotated(Vec2Make(MAX_RAY_LENGTH, 0), angle0-0.001f), vec_player_pos);
            vec2_t intersection1_off = Vec2Add(Vec2Rotated(Vec2Make(MAX_RAY_LENGTH, 0), angle1+0.001f), vec_player_pos);
            bool check_point0 = true, check_point1 = true;
            for(int wall_it_sub = 0; wall_it_sub < sb_count(walls); ++wall_it_sub){
                if(wall_it_sub != wall_it){
                    if(check_point0){
                        vec2_t intersection0_prev = intersection0;
                        if(Vec2IntersectLineSegments(vec_player_pos, intersection0, walls[wall_it_sub].a, walls[wall_it_sub].b, &intersection0)){
                            if(Vec2DistanceBetween(intersection0, intersection0_prev) >= 1.f){
                                check_point0 = false;
                            }
                        }
                        //Vec2IntersectLineSegments(vec_player_pos, intersection0_off, walls[wall_it_sub].a, walls[wall_it_sub].b, &intersection0_off);
                    }
                    else if(!check_point1){
                        break;
                    }
                    if(check_point1){
                        vec2_t intersection1_prev = intersection1;
                        if(Vec2IntersectLineSegments(vec_player_pos, intersection1, walls[wall_it_sub].a, walls[wall_it_sub].b, &intersection1)){
                            if(Vec2DistanceBetween(intersection1, intersection1_prev) >= 1.f){
                                check_point1 = false;
                            }
                        }
                        //Vec2IntersectLineSegments(vec_player_pos, intersection1_off, walls[wall_it_sub].a, walls[wall_it_sub].b, &intersection1_off);
                    }
                }
            }
            if(check_point0 || check_point1){
                for(int wall_it_sub = 0; wall_it_sub < sb_count(walls); ++wall_it_sub){
                    if(check_point0){
                        Vec2IntersectLineSegments(vec_player_pos, intersection0_off, walls[wall_it_sub].a, walls[wall_it_sub].b, &intersection0_off);
                    }
                    if(check_point1){
                        Vec2IntersectLineSegments(vec_player_pos, intersection1_off, walls[wall_it_sub].a, walls[wall_it_sub].b, &intersection1_off);
                    }
                }
            }
            if(check_point0){
                sb_push(ray_angles, angle0-0.001f); sb_push(ray_angles, angle0);
                sb_push(rays, Vec2Sub(intersection0_off, vec_player_pos));
                sb_push(rays, Vec2Sub(intersection0, vec_player_pos));
            }
            if(check_point1){
                sb_push(ray_angles, angle1); sb_push(ray_angles, angle1+0.001f);
                sb_push(rays, Vec2Sub(intersection1, vec_player_pos));
                sb_push(rays, Vec2Sub(intersection1_off, vec_player_pos));
            }
        }
        ProfileTime("Cast rays");
        SortRays(0, sb_count(rays)-1, ray_angles, rays);
        ProfileTime("Sort rays");
        
        KSSetAllPixelComponents(&frame_buffer, 0x00);
        memset(view_mask.pixels, 0x00, view_mask.w*view_mask.h);
        ProfileTime("Clear frame/mask");
        
        if(rays){
            int ray_count = sb_count(rays);
            for(int ray_it = 0; ray_it < ray_count-1; ++ray_it){
                DrawTriToView(&view_mask, &light_sprite, rays, ray_it, ray_it+1);
                //KMDrawTriangle(&view_mask, LIGHT_RADIUS, LIGHT_RADIUS, rays[ray_it].x+LIGHT_RADIUS, rays[ray_it].y+LIGHT_RADIUS,rays[ray_it+1].x+LIGHT_RADIUS, rays[ray_it+1].y+LIGHT_RADIUS, 0xff);
            }
            DrawTriToView(&view_mask, &light_sprite, rays, ray_count-1, 0);
            //KMDrawTriangle(&view_mask, LIGHT_RADIUS, LIGHT_RADIUS, rays[sb_count(rays)-1].x+LIGHT_RADIUS, rays[sb_count(rays)-1].y+LIGHT_RADIUS,rays[0].x+LIGHT_RADIUS, rays[0].y+LIGHT_RADIUS, 0xff);
        }
        ProfileTime("Draw view area on mask");
        
        /*KSBlitMask(&view_mask, &frame_buffer, player.pos.x+20-LIGHT_RADIUS+DRAW_OFFSETX, player.pos.y+21-LIGHT_RADIUS+DRAW_OFFSETY);
        ProfileTime("Draw light");*/
        
        for(int y = 0; y < view_mask.h; ++y) {
            for(int x = 0; x < view_mask.w; ++x) {
                if(view_mask.pixels[y*view_mask.w + x] == 0) continue;
                int dest_pixel_index = (player.pos.y+21-LIGHT_RADIUS+y)*fog_of_war.w + (player.pos.x+20-LIGHT_RADIUS+x);
                uint8_t src_pixel = light_sprite.pixels[y*light_sprite.w + x];
                int dst_pixel = fog_of_war.pixels[dest_pixel_index];
                if(src_pixel > (uint8_t)(dst_pixel>>24)) {
                    uint8_t grey = maze_sprite_grey.pixels[dest_pixel_index];
                    uint8_t faded_grey = grey * ((float)(src_pixel) / 255.f);
                    fog_of_war.pixels[dest_pixel_index] = faded_grey + (faded_grey<<8) + (faded_grey<<16) + (src_pixel<<24);
                }
            }
        }
        //KSBlitMaskToMask(&view_mask, &fog_of_war, player.pos.x+20-LIGHT_RADIUS, player.pos.y+21-LIGHT_RADIUS, KMSetPixelHigher);
        //KSBlitMasked(&maze_sprite, &frame_buffer, &fog_of_war, DRAW_OFFSETX, DRAW_OFFSETY, 0, 0, KSBlendPixel);
        //KSBlitMaskedBlend(&maze_sprite_grey, &fog_of_war, &view_mask, 0, 0, 0, 0);
        ProfileTime("Update fog of war mask");
        KSBlit(&fog_of_war, &frame_buffer, DRAW_OFFSETX, DRAW_OFFSETY);
        ProfileTime("Draw FOW");
        
        { // Blit maze area within line of sight to screen
            int worldx, worldy, x, y;
            uint8_t mask_pixel;
            uint32_t maze_pixel;
            for(y = 0; y < view_mask.h; ++y) {
                worldy = player.pos.y+21-LIGHT_RADIUS+y;
                for(x = 0; x < view_mask.w; ++x) {
                    worldx = player.pos.x+20-LIGHT_RADIUS+x;
                    mask_pixel = Min(view_mask.pixels[y*view_mask.w + x], (uint8_t)light_sprite.pixels[y*light_sprite.w + x]);
                    if(mask_pixel == 0) continue;
                    maze_pixel = maze_sprite.pixels[worldy*maze_sprite.w + worldx];
                    *((uint8_t*)(&maze_pixel) + 3) = mask_pixel;
                    KSBlendPixel(&frame_buffer, worldx+DRAW_OFFSETX, worldy+DRAW_OFFSETY, maze_pixel);
                }
            }
        }
        //KSBlitMasked(&light_sprite, &frame_buffer, &view_mask, player.pos.x+20-300 + DRAW_OFFSETX, player.pos.y+21-300 + DRAW_OFFSETY, player.pos.x+20-300, player.pos.y+21-300, KSSetPixelWithAlpha);
        ProfileTime("Draw light");
#if 0 // Draw walls
        if(draw_mode_walls){
            for(int wall = 0; wall < sb_count(walls); ++wall){
                KSDrawLine(&frame_buffer, walls[wall].a.x + DRAW_OFFSETX, walls[wall].a.y + DRAW_OFFSETY, walls[wall].b.x + DRAW_OFFSETX, walls[wall].b.y + DRAW_OFFSETY, 0xff00ff00, KSSetPixel);
            }
        }
        else {
            /*for(int y = 0; y < maze.h; y++){
                for(int x = 0; x < maze.w; x++){
                    MazeDrawCell(&maze, x, y, &frame_buffer, 0xffffffff);
                }
            }*/
            KSBlitMasked(&maze_sprite, &frame_buffer, &fog_of_war, DRAW_OFFSETX, DRAW_OFFSETY, 0, 0, KSSetPixel);
        }
#endif
        ProfileTime("Draw walls");
        
#if 0 // Draw rays
        for(int ray_it = 0; ray_it < sb_count(rays); ++ray_it){
            KSDrawLinef(&frame_buffer, vec_player_pos.x+DRAW_OFFSETX, vec_player_pos.y+DRAW_OFFSETY, rays[ray_it].x+vec_player_pos.x+DRAW_OFFSETX, rays[ray_it].y+vec_player_pos.y+DRAW_OFFSETY, 0x880000ff, KSSetPixel);
        }
        ProfileTime("Draw rays");
#endif
        
        sb_free(rays);
        sb_free(ray_angles);
        
        KSBlitBlend(&trophy_sprite, &frame_buffer, trophy_pos.x*maze.cell_size + DRAW_OFFSETX, trophy_pos.y*maze.cell_size + DRAW_OFFSETY);
        KSBlitAlpha10(&kero_sprite, &frame_buffer, player.pos.x+1 + DRAW_OFFSETX, player.pos.y + DRAW_OFFSETY);
        ProfileTime("Draw sprites");
        
        if(draw_profiles) {
            profile_frames[current_profile_frame].num_profiles = new_profile_frame.num_profiles;
            for(int profile_it = 0; profile_it < new_profile_frame.num_profiles; ++profile_it){
                profile_frames[current_profile_frame].profiles[profile_it] = new_profile_frame.profiles[profile_it];
            }
            double start_time = new_profile_frame.profiles[0];
            for(int profile_it = 0; profile_it < profile_frames[current_profile_frame].num_profiles; ++profile_it){
                char final_string[128];
                sprintf(final_string, "%.2f %s", profile_frames[current_profile_frame].profiles[profile_it] - (profile_it>0 ? profile_frames[current_profile_frame].profiles[profile_it-1]:start_time), profile_time_names[profile_it]);
                KFDrawColored(&font, &frame_buffer, MAX_PROFILE_FRAMES*2, profile_it*16, final_string, profile_colours[profile_it]);
            }
            
            {
                char final_string[128];
                sprintf(final_string, "%.2f %s", profile_frames[current_profile_frame].profiles[profile_frames[current_profile_frame].num_profiles-1] - profile_frames[current_profile_frame].profiles[0], "Frame time");
                KFDraw(&font, &frame_buffer, MAX_PROFILE_FRAMES*2, profile_frames[current_profile_frame].num_profiles*16, final_string);
            }
            for(int profile_frame_it = 0; profile_frame_it < MAX_PROFILE_FRAMES; ++profile_frame_it){
                double start_time = profile_frames[profile_frame_it].profiles[0];
                for(int profile_frame_profile_it = 1; profile_frame_profile_it < profile_frames[profile_frame_it].num_profiles; ++profile_frame_profile_it){
                    KSDrawLineVerticalSafe(&frame_buffer, profile_frame_it*2, (profile_frames[profile_frame_it].profiles[profile_frame_profile_it-1]-start_time)*10, (profile_frames[profile_frame_it].profiles[profile_frame_profile_it]-start_time)*10, profile_colours[profile_frame_profile_it]);
                }
            }
            ProfileTime("Draw profiles");
        }
        
        KPFlip();
        ProfileTime("Flip");
        new_profile_frame.num_profiles = current_profile_time;
        for(int profile_it = 0; profile_it < current_profile_time; ++profile_it){
            new_profile_frame.profiles[profile_it] = profile_times[profile_it];
        }
        current_profile_frame = (current_profile_frame+1)%MAX_PROFILE_FRAMES;
        current_profile_time = 0;
    }
}
